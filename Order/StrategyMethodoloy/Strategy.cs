﻿using Order.StrategyMethodoloy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.StrategyMethodoloy
{
    abstract class Strategy
    {
        public abstract Task<MethodResult> CreateChargeAsync(MethodRequest model);

        public abstract Task<object> CreateRefundAsync(object model);
    }
}
 