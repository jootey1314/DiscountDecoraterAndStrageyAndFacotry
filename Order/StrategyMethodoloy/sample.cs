﻿using Order.StrategyMethodoloy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.StrategyMethodoloy
{
    class sample
    {
        Context context;
        MethodRequest request = new MethodRequest();
        MethodResult result = new MethodResult();

        public async Task<object> CreateRefundCreateChargeAsync()
        {
            context = new Context(EMethodType.MerchantCommute.ToString());
            return await context.CreateRefundAsync(request);
        }
        public async Task<MethodResult> MerchantCommuteCreateChargeAsync()
        {
            context = new Context(EMethodType.MerchantCommute.ToString());
            return await context.CreateChargeAsync(request);
        }
        public async Task<MethodResult> MerchantDedicatedCreateChargeAsync()
        {
            context = new Context(EMethodType.MerchantDedicated.ToString());
            return await context.CreateChargeAsync(request);
        }
        public async Task<MethodResult> PassengerCommuteCreateChargeAsync()
        {
            context = new Context(EMethodType.PassengerCommute.ToString());
            return await context.CreateChargeAsync(request);
        }

        public async Task<MethodResult> PassengerDedicatedCreateChargeAsync()
        {
            context = new Context(EMethodType.PassengerDedicated.ToString());
            return await context.CreateChargeAsync(request);
        }

    }
}
