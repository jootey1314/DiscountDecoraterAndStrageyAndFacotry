﻿using Order.StrategyMethodoloy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.StrategyMethodoloy
{
    //策略
    class Context
    {
        private Strategy strategy;

        public Context(Strategy st)
        {
            this.strategy = st;
        }
        //工厂
        public Context(string method)
        {
            if (method == EMethodType.MerchantCommute.ToString())
                this.strategy = new MerchantCommuteStrategy();

            if (method == EMethodType.MerchantDedicated.ToString())
                this.strategy = new MerchantDedicatedStrategy();

            if (method == EMethodType.PassengerCommute.ToString())
                this.strategy = new PassengerCommuteStrategy();

            if (method == EMethodType.PassengerDedicated.ToString())
                this.strategy = new PassengerDedicatedStrategy();
        }

        public async Task<MethodResult> CreateChargeAsync(MethodRequest md)
        {
            return await strategy.CreateChargeAsync(md);
        }

        public async Task<object> CreateRefundAsync(object md)
        {
            return await strategy.CreateRefundAsync(md);
        }


    }
}
