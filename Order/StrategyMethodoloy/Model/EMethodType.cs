﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.StrategyMethodoloy.Model
{
    enum EMethodType
    {
        PassengerCommute,
        PassengerDedicated,
        MerchantCommute,
        MerchantDedicated
    }
}
