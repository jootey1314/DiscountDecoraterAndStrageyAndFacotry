﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Decorator.Model
{
    enum EDiscountType
    {
        CouponAndActive,
        Coupon,
        Active,
        Normal
    }
}
