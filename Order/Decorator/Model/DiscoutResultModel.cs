﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Decorator.Model
{
    class DiscoutResultModel
    {
        public long price { set; get; }
        public long discount { set; get; }
        public long prodiscount { set; get; }

    }
}
