﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Decorator.Model
{
    public class DiscountRequestModel
    {
        public string Adcode { set; get; }
        public string CreatorId { set; get; }
        public string OrderId { set; get; }
        public long lineId { set; get; }
        public long CouponId { set; get; }
        public DiscountRequestModel()
        {
            TickectsDate = new List<DataItem>();
        }
        public IEnumerable<DataItem> TickectsDate { set; get; }

        public class DataItem
        {
            public DateTime Date { set; get; }
            public int Price { set; get; }
            public int TickectCount { set; get; }
        }
    }
}

