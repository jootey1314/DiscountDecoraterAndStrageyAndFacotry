﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Order.Decorator.Model;

namespace Order.Decorator
{
    class ActiveDecorator:DiscountDecorator
    {
        private DiscoutResultModel result = new DiscoutResultModel();
        public override DiscoutResultModel CalcPrice(DiscountRequestModel model)
        {
            result = base.CalcPrice(model);
            if (result == null)//todo 表示没有上层装饰，直接返回当前模块的计算结果
                result = result;
            else
                result = result; //todo 按活动价格算出当前的result,然后向减去


            return result;
        }
    }
}
