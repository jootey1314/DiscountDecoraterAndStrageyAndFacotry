﻿using Order.Decorator.Model;

namespace Order.Decorator
{
    //策略
    class DiscountContext
    {
        DiscountDecorator context = null;

        public DiscountContext(DiscountDecorator decorator)
        {
            //装饰之后的类型
            this.context = decorator;
        }

        //工厂模式
        public DiscountContext(string dicountType)
        {
            if (dicountType.ToLower().Equals(EDiscountType.CouponAndActive.ToString().ToLower()))
            {
                var coupon = new CouponDecorator();
                var actvice = new ActiveDecorator();
                actvice.SetComponent(coupon);
                this.context = actvice;
            }
            else if (dicountType.ToLower().Equals(EDiscountType.Coupon.ToString().ToLower()))
            {
                this.context = new CouponDecorator();
            }
            else if (dicountType.ToLower().Equals(EDiscountType.Active.ToString().ToLower()))
            {
                this.context = new ActiveDecorator();
            }
            else
            {
                this.context = new NoDecorator();
            }
        }

        public DiscoutResultModel CalcPrice(DiscountRequestModel model)
        {
            return context.CalcPrice(model);
        }



    }


}
