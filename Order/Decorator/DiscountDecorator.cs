﻿using Order.Decorator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Decorator
{
    abstract class DiscountDecorator:ConcreteCoponent
    {
        private ConcreteCoponent component;
        public void SetComponent(ConcreteCoponent con)
        {
            this.component = con;
        }
        public override DiscoutResultModel CalcPrice(DiscountRequestModel model)
        {
            if (component != null)
            {
              return  component.CalcPrice(model);
            }
            return null; 
        }

    }
}
