﻿using Order.Decorator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Decorator
{
    class Sample
    {

        DiscountContext Op = null;
        DiscoutResultModel result = new DiscoutResultModel();
        DiscountRequestModel request = new DiscountRequestModel();
        public void CalcActivePrice()
        {
            Op = new DiscountContext(Model.EDiscountType.Active.ToString());
            request = new DiscountRequestModel { };
            result = Op.CalcPrice(request);
        }

        public void CalcCouponPrice()
        {
            Op = new DiscountContext(Model.EDiscountType.Coupon.ToString());
            request = new DiscountRequestModel { };
            result = Op.CalcPrice(request);
        }

        public void CalcCouponAndActivePrice()
        {
            Op = new DiscountContext(Model.EDiscountType.CouponAndActive.ToString());
            request = new DiscountRequestModel { };
            result = Op.CalcPrice(request);
        }





    }
}
