﻿using Order.Decorator.CouponStrategy.Model;
using Order.Decorator.Model;

namespace Order.Decorator.CouponStrategy
{
    abstract class Strategy
    {
        public abstract DiscoutResultModel CalcPrice(CouponRequest model);
    }
}

