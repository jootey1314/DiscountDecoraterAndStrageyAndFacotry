﻿using Order.Decorator.CouponStrategy.Model;
using Order.Decorator.Model;

namespace Order.Decorator.CouponStrategy
{
    class Context
    {
        Strategy strategy;

        public Context(string couponType)
        {
            if (couponType == ECouponType.Cash.ToString())
            {
                strategy = new CashStategy();
            }
            else
            if (couponType == ECouponType.Discount.ToString())
            {
                strategy = new DiscountStrategy();
            }
            else
            if (couponType == ECouponType.Fixed.ToString())
            {
                strategy = new FixedStrategy();
            }
        }

        public DiscoutResultModel CalcPrice(CouponRequest model)
        {
            if (strategy != null)
                return strategy.CalcPrice(model);
            return null;
        }
    }
}
