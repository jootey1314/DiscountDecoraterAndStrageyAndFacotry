﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Decorator.CouponStrategy.Model
{
    enum ECouponType
    {
        Cash,
        Fixed,
        Discount
    }
}
