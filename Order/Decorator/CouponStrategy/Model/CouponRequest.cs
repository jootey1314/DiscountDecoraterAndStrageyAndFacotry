﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Decorator.CouponStrategy.Model
{
    class CouponRequest
    {
        public int Price { set; get; }
        public int CouponPrice { set; get; }
    }
}
