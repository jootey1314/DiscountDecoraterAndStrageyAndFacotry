﻿using Order.Decorator.CouponStrategy.Model;
using Order.Decorator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Decorator
{
    class CouponDecorator : DiscountDecorator
    {
        private DiscoutResultModel result = new DiscoutResultModel();
        public override DiscoutResultModel CalcPrice(DiscountRequestModel model)
        {
            result = base.CalcPrice(model);
            if (result == null) //todo 按优惠券价格算出当前的result,然后向减去
                result = result;
            else
            {
                //todo 调用sdk获取优惠券信息
                var couponId = model.CouponId;

                //var couponType=GetCoupont(couponId).kind;

                var request = new CouponRequest
                {
                    CouponPrice = 10,
                    Price = 100
                };
                //计算价格
                var Op = new CouponStrategy.Context(ECouponType.Cash.ToString());               
             
                result = Op.CalcPrice(request);
            }

            return result;
        }


    }
}
