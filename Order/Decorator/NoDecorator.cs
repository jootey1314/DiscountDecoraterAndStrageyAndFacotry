﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Order.Decorator.Model;

namespace Order.Decorator
{
    class NoDecorator : DiscountDecorator
    {
        private DiscoutResultModel result = new DiscoutResultModel();
        public override DiscoutResultModel CalcPrice(DiscountRequestModel model)
        {
            //todo 查询出原价返回
            result = new DiscoutResultModel
            {
                price = 10,
                discount = 0,
                prodiscount = 10
            };
            return result;
        }
    }
}
